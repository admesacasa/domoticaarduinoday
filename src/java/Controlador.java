
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author adrianomesacasa
 */
@Named("controlador")
@SessionScoped
public class Controlador implements Serializable {

    private String temperaturaEumidade;

    private String rele1 = "1";
    private String rele2 = "3";
    private String rele3 = "5";

    private String botaRele1 = "Desligado";
    private String botaRele2 = "Desligado";
    private String botaRele3 = "Desligado";
    private String ventilador = "Desligado";

    SerialClass main = new SerialClass();

    @PostConstruct
    public void postConstruct() {

        main.initialize();

    }

    private static BufferedReader input;

    public void leTemperaura() {
        try {
            input = SerialClass.input;
            String inputLine = input.readLine();
            Long temeratura = Long.parseLong(inputLine.substring(8, 10));
            if (temeratura >= 29) {
                ventilador = "Ligado";
            } else {
                ventilador = "Desligado";
            }
            temperaturaEumidade = inputLine;
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void comandaRele1() {
        if (rele1.equals("0")) {
            botaRele1 = "Desligado";
            rele1 = "1";
            main.writeData(rele1);

        } else {
            botaRele1 = "Ligado";
            rele1 = "0";
            main.writeData(rele1);
        }

    }

    public void comandaRele2() {
        if (rele2.equals("2")) {
            botaRele2 = "Desligado";
            rele2 = "3";
            main.writeData(rele2);
        } else {
            botaRele2 = "Ligado";
            rele2 = "2";
            main.writeData(rele2);

        }
    }

    public void comandaRele3() {
        if (rele3.equals("4")) {
            botaRele3 = "Desligado";
            rele3 = "5";
            main.writeData(rele3);
        } else {
            botaRele3 = "Ligado";
            rele3 = "4";
            main.writeData(rele3);

        }

    }

    public String getTemperaturaEumidade() {
        return temperaturaEumidade;
    }

    public void setTemperaturaEumidade(String temperaturaEumidade) {
        this.temperaturaEumidade = temperaturaEumidade;
    }

    public String getRele1() {
        return rele1;
    }

    public void setRele1(String rele1) {
        this.rele1 = rele1;
    }

    public String getRele2() {
        return rele2;
    }

    public void setRele2(String rele2) {
        this.rele2 = rele2;
    }

    public String getRele3() {
        return rele3;
    }

    public void setRele3(String rele3) {
        this.rele3 = rele3;
    }

    public String getBotaRele1() {
        return botaRele1;
    }

    public void setBotaRele1(String botaRele1) {
        this.botaRele1 = botaRele1;
    }

    public String getBotaRele2() {
        return botaRele2;
    }

    public void setBotaRele2(String botaRele2) {
        this.botaRele2 = botaRele2;
    }

    public String getBotaRele3() {
        return botaRele3;
    }

    public void setBotaRele3(String botaRele3) {
        this.botaRele3 = botaRele3;
    }

    public String getVentilador() {
        return ventilador;
    }

    public void setVentilador(String ventilador) {
        this.ventilador = ventilador;
    }

}
