#include "DHT.h"

#define DHTTYPE DHT22
#define DHTPIN 7    
int rele1 = 3;
int rele2 = 5;
int rele3 = 9;
int rele4 = 8;
char letra;
int val = 0;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
    Serial.begin(9600); 
    //Serial.println("DHTxx test!");
    pinMode(rele1, OUTPUT);
    pinMode(rele2, OUTPUT);
    pinMode(rele4, OUTPUT);
    pinMode(rele3, OUTPUT);
    dht.begin();
}

void loop() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();
       if (isnan(t) || isnan(h)) {
           Serial.println("Failed to read from DHT");
       } else {
          Serial.print(h);
          Serial.print(" %\t");
          Serial.print(t);
          Serial.println(" *C");
      }
      if(t>=29){
         digitalWrite(rele1, HIGH);
      }else{
         digitalWrite(rele1, LOW); 
    }
}

  void serialEvent()
  {
   {
     val = Serial.parseInt();
    if(val == 0){ 
       digitalWrite(rele4, HIGH);
    }else if(val == 1) {
       digitalWrite(rele4, LOW); 
    }else if(val == 2) {
       digitalWrite(rele2, HIGH);
    } else if(val == 3) {
       digitalWrite(rele2, LOW);  
    }else if(val == 4) {
       digitalWrite(rele3, HIGH);
    }else if(val == 5) {
       digitalWrite(rele3, LOW); 
   }
 }
}
  
